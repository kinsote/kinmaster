import React, { useState, useEffect } from 'react';

const MainRanking = () => {
	const [ ranking, setRanking ] = useState({});

	useEffect(() => {
		fetch('http://localhost:8080/ranking?game=SCAPE_ROOM', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then((res) => res.json())
			.then((res) => setRanking({ ranking: res }));
	}, []);

	const renderTableData = () => {
		if (Array.isArray(ranking.ranking)) {
			//console.log(ranking);

			return ranking.ranking.map((rank, index) => {
				const { id, username, score } = rank;
				return (
					<tr key={id}>
						<td>{username}</td>
						<td>{score} seg</td>
					</tr>
				);
			});
		} else {
			return (
				<tr>
					<td />
					<td />
				</tr>
			);
		}
	};

	return (
		<section className="ranking">Escape Room
			<table className="tabla-rank">
				<thead>
					<tr className="user-rank">
						<th >Nombre </th>
						<th>Tiempo</th>
					</tr>
				</thead>
				<tbody className="data-rank">{renderTableData()}</tbody>
			</table>

			<div className="clearfix" />
		</section>
	);
};
export default MainRanking;
