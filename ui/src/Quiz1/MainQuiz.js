import React from 'react';
import { quizData } from './quizData';
import '../assets/css/Quiz.css';
import { NavLink } from 'react-router-dom';

class MainQuiz extends React.Component {
	state = {
		currentQuestion: 0,
		myAnswer: null,
		options1: [],
		score: 0,
		disabled: true,
		isEnd: false
	};

	loadQuizData = () => {
		this.setState(() => {
			return {
				questions: quizData[this.state.currentQuestion].question,
				answer: quizData[this.state.currentQuestion].answer,
				options1: quizData[this.state.currentQuestion].options1,
				score: 0
			};
		});
	};

	componentDidMount() {
		this.loadQuizData();
	}

	updateScore = (answer, myAnswer) => {
		const { score } = this.state;
		if (myAnswer.toString() === answer.toString()) {
			console.log('CORRECTO');
			this.setState({
				score: score + 1
			});
		}
	};

	nextQuestionHandler = () => {
		const { myAnswer, answer } = this.state;
		this.updateScore(answer, myAnswer);
		this.setState({
			currentQuestion: this.state.currentQuestion + 1
		});
	};

	componentDidUpdate(prevProps, prevState) {
		if (this.state.currentQuestion !== prevState.currentQuestion) {
			this.setState(() => {
				return {
					disabled: true,
					questions: quizData[this.state.currentQuestion].question,
					options1: quizData[this.state.currentQuestion].options1,
					answer: quizData[this.state.currentQuestion].answer
				};
			});
		}
	}
	checkAnswer = (answer) => {
		this.setState({ myAnswer: answer, disabled: false });
	};

	finishHandler = () => {
		const { myAnswer, answer } = this.state;
		this.updateScore(answer, myAnswer);
		if (this.state.currentQuestion === quizData.length - 1) {
			this.setState({
				isEnd: true
			});
		}
	};

	render() {
		const { options1, myAnswer, currentQuestion, isEnd } = this.state;

		if (isEnd) {
			const data = {
				username: JSON.parse(localStorage.getItem('user')).name,
				score: this.state.score,
				game: 'QUIZ'
			};

			fetch('http://localhost:8080/score', {
				method: 'POST',
				body: JSON.stringify(data),
				headers: {
					'Content-Type': 'application/json'
				}
			});

			return (
				<div className="container-quiz1">
					<div className="App-quiz">
						<div className="result">
							<h3>Tu puntuacion final es... {this.state.score} puntos </h3>
							<p>Las opciones correctas eran las siguientes:</p>
							<ol className="ul-quiz">
								{quizData.map((item, index) => (
									<li className="li-quiz" key={index.id}>
										{item.answer}
									</li>
								))}
							</ol>
							<NavLink to="/home" className="next-quiz">
								Salir
							</NavLink>
						</div>
					</div>
				</div>
			);
		} else {
			return (
				<div className="container-quiz1">
					<div className="App-quiz">
						<h1 className="h1-quiz">{this.state.questions} </h1>
						<span>{`Pregunta ${currentQuestion + 1} de ${quizData.length}  `}</span>
						{options1.map((option) => (
							<span key={option} className="optquiz">
								<p
									className={`ui floating message options1 ${myAnswer === option
										? 'selected1'
										: null}`}
									onClick={() => this.checkAnswer(option)}
								>
									{' '}
									{option}
								</p>
							</span>
						))}
						{currentQuestion < quizData.length - 1 && (
							<button
								className="next-quiz"
								disabled={this.state.disabled}
								onClick={this.nextQuestionHandler}
							>
								Siguiente
							</button>
						)}
						{currentQuestion === quizData.length - 1 && (
							<button className="next-quiz" onClick={this.finishHandler}>
								Finalizar
							</button>
						)}
					</div>
				</div>
			);
		}
	}
}

export default MainQuiz;
