const { Pool } = require('pg');

const pool = new Pool({
	host: 'localhost',
	user: 'postgres',
	max: 20,
	idleTimeoutMillis: 30000,
	connectionTimeoutMillis: 2000,
	password: '123456',
	port: 5432,
	database: 'skape'
});

const saveUser = async (name, password, birthday, email, avatar) => {
	const client = await pool.connect();

	const selectResult = await pool.query(`select * from usuario where email = '${email}'`);

	if (selectResult.rows.length !== 0) {
		throw 'usuario repetido';
	}

	const user = await pool.query(`insert into usuario (name, birthday, password, email, avatar)
    values ('${name}','${birthday}','${password}','${email}','${avatar}') RETURNING id`);

	client.release();

	return user;
};

//Type - scape, quiz
const saveRanking = async (username, score, game) => {
	const client = await pool.connect();

	const rank = await pool.query(`insert into ranking (username, score, game)
    values ('${username}','${score}', '${game}')`);

	client.release();

	return rank;
};

const getRankingForGame = async (game) => {
	const client = await pool.connect();
	let orderBy = 'desc';

	if (game === 'SCAPE_ROOM') {
		orderBy = 'asc';
	}
	const query = `select * from ranking where game = '${game}' order by score ${orderBy}`;

	//console.log(query)
	const results = await client.query(query);
	client.release();
	return results.rows;
};

const saveScore = async (username, score, game) => {
	const client = await pool.connect();

	const rank = await pool.query(`insert into ranking (username, score, game)
	  values ('${username}','${score}', '${game}')`);

	client.release();

	return rank;
};

const getScoreForGame = async (game) => {
	const client = await pool.connect();
	let orderBy = 'asc';

	if (game === 'QUIZ') {
		orderBy = 'desc';
	}
	const query = `select * from ranking where game = '${game}' order by score ${orderBy}`;

	//console.log(query)
	const results = await client.query(query);
	client.release();
	return results.rows;
};

const getUserById = async (id) => {
	const client = await pool.connect();
	const query = `select * from usuario where id = '${id}'`;
	const results = await client.query(query, [ id ]);
	client.release();
	return results;
};
const editUser = async (id, name, birthday, email, avatar) => {
	//password
	//console.log(('PRUEBA',id, name, birthday, email, avatar))
	const client = await pool.connect();
	const query = `update usuario set name = $2,  birthday = $3, email = $4, avatar = $5 where id = $1`; // password = $3,
	const result = await client.query(query, [ id, name, birthday, email, avatar ]); //password
	//console.log(result)
	client.release();
	result;
};
const deleteUser = async (id) => {
	const client = await pool.connect();
	let result;

	try {
		result = await client.query(`delete from usuario where id='${id}'`);
	} catch (err) {
		console.error(err);
		return false;
	}

	client.release();

	if (result.rowCount === 0) {
		return false;
	}

	return true;
};

const getUserByname = async (name) => {
	const client = await pool.connect();

	const resultSelect = await client.query(`select *
    from usuario where name='${name}'`);

	if (resultSelect.rows.length === 0) {
		throw Error('unknown-user');
	}
	return resultSelect.rows[0];
};

module.exports = {
	saveUser,
	editUser,
	getUserByname,
	getUserById,
	deleteUser,
	saveRanking,
	getRankingForGame,
	saveScore,
	getScoreForGame
};
