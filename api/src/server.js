require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const url = require('url');
var cors = require('cors');

const moment = require('moment');

const stPg = require('./storage_pg');

let validTokens = {};

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('public'));
app.use(cors());

const BCRYPT_SALT_ROUNDS = 12;

const createToken = () => {
	return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
};

const saveToken = (token) => {
	validTokens[token] = {
		expires: moment().add(5, 'h'),
		rol: 'user'
	};
};

app.post('/registro', function(req, res, next) {
	const name = req.body.name;
	const password = req.body.password;
	const birthday = req.body.birthday;
	const email = req.body.email;
	const avatar = req.body.avatar;

	bcrypt
		.hash(password, BCRYPT_SALT_ROUNDS)
		.then(function(hashedPassword) {
			stPg
				.saveUser(name, hashedPassword, birthday, email, avatar)
				.then((user) => {
					//console.log('USER SAVED', req.body);
					const userCreated = req.body;
					const token = createToken();
					saveToken(token);
					userCreated.token = token;
					delete userCreated.password;
					res.status(201).send({ ...userCreated, id: user.rows[0].id });
				})
				.catch(function(error) {
					res.status(400).send(error);
				});
		})
		.catch(function(error) {
			res.status(400).send();
			console.log('Error saving user: ');
			console.log(error);
			next();
		});
});

app.post('/login', function(req, res, next) {
	const name = req.body.name;
	const password = req.body.password;
	let id;
	let userData;
	const createToken = () => {
		return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
	};

	stPg
		.getUserByname(name)
		.then(function(user) {
			//console.log(user);
			userData = user;
			return bcrypt.compare(password, user.password);
		})
		.then(function(samePassword) {
			if (!samePassword) {
				res.status(403).send();
			}

			const token = createToken();
			saveToken(token);
			userData.token = token;
			delete userData.password;
			res.json(userData);
		})
		.catch(function(error) {
			console.log('Error authenticating user: ');
			console.log(error);
			res.status(403).send();
			next();
		});
});

app.post('/ranking', function(req, res, next) {
	const username = req.body.username;
	const score = req.body.score;
	const game = req.body.game;
	stPg
		.saveRanking(username, score, game)
		.then(function() {
			res.status(200).send();
		})
		.catch(function(error) {
			console.log('Error post ranking: ');
			console.log(error);
			res.status(403).send();
			next();
		});
});

app.get('/ranking', function(req, res, next) {
	const queryObject = url.parse(req.url, true).query;
	const game = queryObject.game;
	//console.log(queryObject)

	stPg
		.getRankingForGame(game)
		.then(function(ranking) {
			res.status(200).send(ranking);
		})
		.catch(function(error) {
			console.log('Error post ranking: ');
			console.log(error);
			res.status(403).send();
			next();
		});
});

app.post('/score', function(req, res, next) {
	const username = req.body.username;
	const score = req.body.score;
	const game = req.body.game;
	stPg
		.saveScore(username, score, game)
		.then(function() {
			res.status(200).send();
		})
		.catch(function(error) {
			console.log('Error post score: ');
			console.log(error);
			res.status(403).send();
			next();
		});
});

app.get('/score', function(req, res, next) {
	const queryObject = url.parse(req.url, true).query;
	const game = queryObject.game;
	//console.log(queryObject)

	stPg
		.getScoreForGame(game)
		.then(function(score) {
			res.status(200).send(score);
		})
		.catch(function(error) {
			console.log('Error post score: ');
			console.log(error);
			res.status(403).send();
			next();
		});
});

app.put('/editUser', async (req, res) => {
	///:id para postman
	let id = req.body.id;
	//console.log('USER EDIT',req.body)

	const name = req.body.name;
	//const password = req.body.password;
	const birthday = req.body.birthday;
	const email = req.body.email;
	const avatar = req.body.avatar;

	try {
		//const BCRYPT_SALT_ROUNDS = 12;
		//const hashedPassword = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS);
		await stPg.editUser(id, name, birthday, email, avatar); //hashedPassword,

		res.status(200).json();
	} catch (e) {
		console.log(e.message);
		res.status(400).send(e);
	}
});

app.delete('/borrar/:id', async (req, res) => {
	let id = req.params.id;
	let isDeleted = false;

	try {
		isDeleted = await stPg.deleteUser(id);
	} catch (err) {
		console.log(err);
	}

	if (!isDeleted) {
		res.status(400).send();
		return;
	}

	res.status(200).send();
});

const port = process.env.PORT || 8080;
app.listen(port, () => {
	console.log(`API disponible en: http://localhost:${port}`);
});
